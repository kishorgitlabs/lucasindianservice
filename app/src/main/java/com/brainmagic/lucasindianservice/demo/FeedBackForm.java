package com.brainmagic.lucasindianservice.demo;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.creativityapps.gmailbackgroundlibrary.BackgroundMail;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.myhexaville.smartimagepicker.ImagePicker;
import com.myhexaville.smartimagepicker.OnImagePickedListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import alertbox.Alertbox;
import api.retrofit.APIService;
import dlr.DLR_Registeration_Activity;
import model.uploadimage.UploadImage;
import network.NetworkConnection;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class FeedBackForm extends AppCompatActivity  {


    private MaterialSpinner feedbacktype;
    private ArrayList<String> feedbacktypes;
    private EditText feedbackgiven;
    private Button feedbacksubmit,addimage;
    private String selectedfeedbacktype,feedbackgivenbyuser,username,usertype,usermobile,User_EmailId;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private ProgressDialog progressDialog;
    private ImagePicker imagePicker;
    private ImageView selectedimage;
    private String mImageName = "";
    private File mImageFile;
    String imagename="";
    private Uri i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back_form);
        myshare = getSharedPreferences("LIS", MODE_PRIVATE);

        username= myshare.getString("UserName", "");
        usertype= myshare.getString("UserType", "");
        usermobile= myshare.getString("Mobile", "");
        feedbacktype = (MaterialSpinner) findViewById(R.id.feedbacktype);
        feedbackgiven=findViewById(R.id.feedbackgiven);
        selectedimage=findViewById(R.id.selectedimage);
        feedbacksubmit=findViewById(R.id.feedbacksubmit);
        addimage=findViewById(R.id.addimage);
        feedbacktype.setBackgroundResource(R.drawable.background_spinner);
        feedbacktypes = new ArrayList<>();
        feedbacktypes.add("Select Complaint Type");
        feedbacktypes.add("Feedback");
        feedbacktypes.add("Complaint");
        feedbacktypes.add("Issue");
        feedbacktype.setItems(feedbacktypes);
         User_EmailId = getEmiailID(getApplicationContext());

        feedbacktype.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner materialSpinner, int i, long l, Object o) {
                selectedfeedbacktype=o.toString();
            }
        });

        feedbacksubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                feedbackgivenbyuser=feedbackgiven.getText().toString();
                if (selectedfeedbacktype.equals("Select Complaint Type")&&!selectedfeedbacktype.equals(null)){
                    StyleableToasty styleableToasty = new StyleableToasty(FeedBackForm.this);
                    styleableToasty.showSuccessToast("Please select complaint type");
                }else if (TextUtils.isEmpty(feedbackgivenbyuser)){
                    StyleableToasty styleableToasty = new StyleableToasty(FeedBackForm.this);
                    styleableToasty.showSuccessToast("Feedback cannot be empty");
                }else {
                    checkInternet();
                }

            }
        });

        addimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagePicker=new ImagePicker(FeedBackForm.this, null, new OnImagePickedListener() {
                    @Override
                    public void onImagePicked(Uri imageUri) {
                        selectedimage.setImageURI(imageUri);
                        mImageName = getFileName(imageUri);
                        mImageFile = getFileFromImage();
                        i=imageUri;

                    }
                });
                imagePicker.choosePicture(true);
            }
        });
        requespremission();

    }

    private void uploadimage(File selectimage) {
        try {
            progressDialog = new ProgressDialog(FeedBackForm.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Sending email...");
            progressDialog.show();

            CategoryAPI service = RetroClient.getApiServiceUpload();
            String type="image/png";
            imagename=selectimage.getName();
            RequestBody requestBody = RequestBody.create(MediaType.parse(type), selectimage);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", selectimage.getName(), requestBody);
            final Call<UploadImage> request = service.uploadimage(filePart);
            request.enqueue(new Callback<UploadImage>() {
                @Override
                public void onResponse(Call<UploadImage> call, Response<UploadImage> response) {
                    if (response.body().getMessage().equals("Success.")) {
                        progressDialog.dismiss();
//                        uploaduserdetails();
                    }
//                                else {
//                                    Nointernet("Please Try Again later");
//                                }
                }
                @Override
                public void onFailure(Call<UploadImage> call, Throwable t) {
                    Log.v("Upload Exception", t.getMessage());
                    progressDialog.dismiss();
                    t.printStackTrace();
                }
            });
        }
        catch (Exception ex) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
            Log.v("Exception", ex.getMessage());

        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private File getFileFromImage() {
        try {
            BitmapDrawable drawable = (BitmapDrawable) selectedimage.getDrawable();
            Bitmap bitmap = drawable.getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            File directory = new File(getFilesDir(), "profile");
            if (!directory.exists())
                directory.mkdirs();
            File myappFile = new File(directory
                    + File.separator + mImageName);
            FileOutputStream fos = new FileOutputStream(myappFile);
            fos.write(byteArray);
//                        mImageName = File_URL + myappFile.getName();
            return myappFile;
        } catch (Exception e) {
            e.printStackTrace();
            return new File("");
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode==9&&grantResults[0]==PackageManager.PERMISSION_GRANTED){

        }
        else {
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            final AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialogBuilder.setMessage("Please Allow The Camera Permission To AttachFile ");
            alertDialogBuilder.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            requespremission();
                        }
                    });

            alertDialogBuilder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    requespremission();
                }
            });
            alertDialogBuilder.show();
        }
    }

    private void requespremission() {

        if (ContextCompat.checkSelfPermission(FeedBackForm.this,Manifest.permission.CAMERA)== PackageManager.PERMISSION_GRANTED){


        }else {
            ActivityCompat.requestPermissions(FeedBackForm.this,new String[]{Manifest.permission.CAMERA},9 );
        }
    }



    private void checkInternet() {

        NetworkConnection connection = new NetworkConnection(this);
        if (connection.CheckInternet())
            SendMail();
        else
            alertnew(getResources().getString(R.string.no_internet));
    }

    public void alertnew(String s) {

        Alertbox alert = new Alertbox(FeedBackForm.this);
        alert.newalert(s);
    }



    private String getEmiailID(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);
        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }
    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }


    protected void sendEmail() {
        Log.i("Send email", "");

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"balaji@brainmagic.info"});
        // emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, selectedfeedbacktype);
        String msg = " Name         : " + username + " \n Mobile No    : " + usermobile + "\n User Type    : " + usertype
                + "\n Assistance Required  : " + feedbackgivenbyuser;
        Log.v("Body", msg);

        emailIntent.putExtra(Intent.EXTRA_TEXT, msg);

        try {
            startActivity(Intent.createChooser(emailIntent, "Sending email..."));
            finish();
            Log.i("Finish sending email...", "");
        } catch (android.content.ActivityNotFoundException ex) {
            //Toast.makeText(AskWabcoActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
            alertnew("You don't have a default email configured in your mobile. Cannot attach file !");
        }
    }

    public void SendMail() {
        progressDialog = new ProgressDialog(FeedBackForm.this);
         progressDialog.setCancelable(false);
        progressDialog.setMessage("Sending email...");
        progressDialog.show();

        String msg = " Name         : " + username + " \n Mobile No    : " + usermobile + "\n User Type    : " + usertype
                + "\n Assistance Required  : " + feedbackgivenbyuser;
        Log.v("Body", msg);
        String subject = selectedfeedbacktype;
        // String mail = "customer.care@wabco-auto.com,sivakumar.s@wabco-auto.com,rkumaravel@brainmagic.info";
        String mail = "balaji@brainmagic.info";
        try {
            BackgroundMail backgroundMail =   BackgroundMail.newBuilder(FeedBackForm.this)
                    .withUsername(User_EmailId)
                    .withPassword("Suk37q$8")
                    .withMailto(mail)
                    .withType(BackgroundMail.TYPE_PLAIN)
                    .withSubject(subject)
                    .withBody(msg)
                    .withOnSuccessCallback(new BackgroundMail.OnSuccessCallback() {
                        @Override
                        public void onSuccess() {
                            //do some magic
                            progressDialog.dismiss();
                            alertnew("Your mail has been sent successfully !");
                        }
                    })
                    .withOnFailCallback(new BackgroundMail.OnFailCallback() {
                        @Override
                        public void onFail() {
                            //do some magic
                            progressDialog.dismiss();
                            //alertbox.showAlertbox("Your mail was not sent!");
                            sendEmail();
                        }
                    })
                    .send();



        } catch (Exception e) {
            progressDialog.dismiss();
            alertnew(e.toString());
        }


    }

}
