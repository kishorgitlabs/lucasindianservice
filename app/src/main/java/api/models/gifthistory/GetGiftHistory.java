
package api.models.gifthistory;

import java.util.List;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class GetGiftHistory {

    @SerializedName("data")
    private List<GetHistoryData> mData;
    @SerializedName("result")
    private String mResult;

    public List<GetHistoryData> getData() {
        return mData;
    }

    public void setData(List<GetHistoryData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
