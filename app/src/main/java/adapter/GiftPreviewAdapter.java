package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.R;

import java.util.List;

import giftcatalogue.RedeemPoints;

public class GiftPreviewAdapter extends ArrayAdapter  {


    private Context context;
    private List<String> ggcode;
    private List<String> ggname;
    private List<String> ggpoints;

    public GiftPreviewAdapter(@NonNull Context context,List<String> gcodes, List<String> gnames, List<String> gpointss) {
        super(context, R.layout.giftselectedadapterpreview);
        this.context=context;
        this.ggcode=gcodes;
        this.ggname=gnames;
        this.ggpoints=gpointss;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view= LayoutInflater.from(context).inflate(R.layout.giftselectedadapterpreview, parent,false);

        TextView viewgiftcode=view.findViewById(R.id.viewgiftcode);
        TextView viewgiftname=view.findViewById(R.id.viewgiftname);
        TextView viewpoints=view.findViewById(R.id.viewpoints);

        viewgiftcode.setText(ggcode.get(position));
        viewgiftname.setText(ggname.get(position));
        viewpoints.setText(ggpoints.get(position));

        return view;
    }

    @Override
    public int getCount() {
        return ggcode.size();
    }
}
