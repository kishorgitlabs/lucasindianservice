package adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.lucasindianservice.demo.AddtoCartActivity;
import com.brainmagic.lucasindianservice.demo.OverallSearch;
import com.brainmagic.lucasindianservice.demo.PartDetails;
import com.brainmagic.lucasindianservice.demo.ProductCatalogue;
import com.brainmagic.lucasindianservice.demo.R;

import java.util.List;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import api.models.overallsearch.OverallSearchData;
import model.PartModelClass;
import model.PartNoListDetails;
import model.PartNoSearch;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OverallSearchAdapter extends ArrayAdapter {
    Context context;
    private List<OverallSearchData> data;
    private PartModelClass modelClass;
    private String s;


    public OverallSearchAdapter(@NonNull Context context, List<OverallSearchData> data, String searchedstring) {
        super(context, R.layout.overallsearchadapter);
        this.context=context;
        this.data=data;
        this.s=searchedstring;
    }
    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView=null;
        if (convertView==null) {
            convertView=LayoutInflater.from(context).inflate(R.layout.overallsearchadapter, null);
            TextView sino1_producto=convertView.findViewById(R.id.sino1_producto);
            TextView partno_producto=convertView.findViewById(R.id.partno_producto);
            TextView product_desco=convertView.findViewById(R.id.product_desco);
            TextView product_speco=convertView.findViewById(R.id.product_speco);
            ImageView product_cart=convertView.findViewById(R.id.product_cart);
            sino1_producto.setText(position+1+"");
            partno_producto.setText(data.get(position).getPartNo());
            product_desco.setText(data.get(position).getDescription());
            product_speco.setText("Spec");

            if (data.get(position).getPartNo().contains(s)) {
                int startPos = data.get(position).getPartNo().indexOf(s);
                int endPos = startPos + s.length();
                int color= ContextCompat.getColor(getContext(),R.color.green);
                Spannable spanText = Spannable.Factory.getInstance().newSpannable(data.get(position).getPartNo()); // <- EDITED: Use the original string, as `country` has been converted to lowercase.
                spanText.setSpan(new ForegroundColorSpan(color), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                partno_producto.setText(spanText, TextView.BufferType.SPANNABLE);

            }
            else if (data.get(position).getDescription().toLowerCase().contains(s.toLowerCase())) {
                int startdes = data.get(position).getDescription().toLowerCase().indexOf(s.toLowerCase());
                int enddes = startdes + s.length();
                int color = ContextCompat.getColor(getContext(), R.color.green);
                Spannable spanText = Spannable.Factory.getInstance().newSpannable(data.get(position).getDescription()); // <- EDITED: Use the original string, as `country` has been converted to lowercase.
                spanText.setSpan(new ForegroundColorSpan(color), startdes, enddes, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                product_desco.setText(spanText, TextView.BufferType.SPANNABLE);
            }

            product_speco.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String partNumber=data.get(position).getPartNo().toString();
                    context.startActivity(new Intent(context, PartDetails.class).putExtra("Partnumber",partNumber));
                }
            });
            product_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkInternet(data.get(position).getPartNo());
                }
            });
        }
        return convertView;
    }
    @Override
    public int getCount() {
        return data.size();
    }

    private void checkInternet(String partNumber) {
        NetworkConnection net = new NetworkConnection(context);
        if (net.CheckInternet()) {
            getpartdetails(partNumber);
        } else {
            Toast.makeText(context, "Please check your network connection and try again!", Toast.LENGTH_SHORT).show();
        }
    }
    private void getpartdetails(final String partNumber) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(context,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();
            Call<PartNoSearch> call = service.partNoSearch(partNumber);
            call.enqueue(new Callback<PartNoSearch>() {
                @Override
                public void onResponse(Call<PartNoSearch> call, Response<PartNoSearch> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("success")) {
                        List<PartNoListDetails> PartDetails = response.body().getData();
                        modelClass=new PartModelClass();
                        modelClass.setPartNo(PartDetails.get(0).getPartNo());
                        modelClass.setAppName(PartDetails.get(0).getAppname());
                        modelClass.setDescription(PartDetails.get(0).getDescription());
                        modelClass.setOemPartNo(PartDetails.get(0).getOemPartno());
                        modelClass.setProStatus(PartDetails.get(0).getProStatus());
                        modelClass.setProType(PartDetails.get(0).getProType());
                        modelClass.setMrp(PartDetails.get(0).getMrp());
                        modelClass.setProductName(PartDetails.get(0).getProductname());
                        modelClass.setPartVolt(PartDetails.get(0).getPartVolt());
                        modelClass.setPartOutputrng(PartDetails.get(0).getPartOutputrng());
                        modelClass.setProModel(PartDetails.get(0).getProModel());
                        modelClass.setProSupersed(PartDetails.get(0).getProSupersed());
                        modelClass.setPartimage(PartDetails.get(0).getmPartImage());
                        context.startActivity(new Intent(context, AddtoCartActivity.class).putExtra("addtoCart",modelClass));
                    } else {
                        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                                context).create();
                        View dialogView=LayoutInflater.from(context).inflate(R.layout.empty_part_alert,null);
//                        View dialogView = inflater.inflate(R.layout.empty_part_alert, null);
                        alertDialog.setView(dialogView);
                        Button Ok = (Button) dialogView.findViewById(R.id.ok);
                        final TextView Message = (TextView) dialogView.findViewById(R.id.msg);
//                    final TextView send = (TextView) dialogView.findViewById(R.id.toenquiry);
                        Message.setText("No Parts Available !");

                        Ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                context.startActivity(new Intent(context, ProductCatalogue.class));
                                alertDialog.dismiss();
                            }
                        });
//            send.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    startActivity(new Intent(PartDetails.this, SendEnquiry.class));
//                    alertDialog.dismiss();
//                }
//            });
                        alertDialog.show();
                        progressDialog.dismiss();
                        Toast.makeText(context, "No Record Found", Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<PartNoSearch> call, Throwable t) {
                    final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                            context).create();

//                    LayoutInflater inflater = (context).
                    View dialogView=LayoutInflater.from(context).inflate(R.layout.empty_part_alert,null);
//                    View dialogView = inflater.inflate(R.layout.empty_part_alert, null);
                    alertDialog.setView(dialogView);
                    Button Ok = (Button) dialogView.findViewById(R.id.ok);
                    final TextView Message = (TextView) dialogView.findViewById(R.id.msg);
//                final TextView send = (TextView) dialogView.findViewById(R.id.toenquiry);
                    Message.setText("No Parts Available !");

                    Ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            context.startActivity(new Intent(context, ProductCatalogue.class));
                            alertDialog.dismiss();
                        }
                    });
//                send.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        startActivity(new Intent(Partdetails.this, SendEnquiry.class));
//                        alertDialog.dismiss();
//                    }
//                });
                    alertDialog.show();
                    progressDialog.dismiss();
                    Toast.makeText(context, "Someting went Wrong", Toast.LENGTH_LONG).show();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Toast.makeText(context, "Please try again", Toast.LENGTH_LONG).show();
        }
    }

}
