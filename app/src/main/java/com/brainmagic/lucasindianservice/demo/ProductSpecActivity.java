package com.brainmagic.lucasindianservice.demo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.ProductTypeAdapter;
import alertbox.Alertbox;
import home.Home_Activity;
import model.RatingList;
import model.TypeList;
import model.VoltageList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class ProductSpecActivity extends AppCompatActivity {
    private boolean visible,visibles;

    private ProgressDialog  loading;
    private MaterialSpinner voltagespin,ratingspin,typespin;
    List<String> ratingspinlist,typespinlist;
    private  ViewGroup lL;
    private ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_spec);

        Intent intent=getIntent();
//        String product_detail=intent.getExtras().getString("category");
        boolean isRating=intent.getBooleanExtra("rating",false);
        boolean isType=intent.getBooleanExtra("type",false);

        final String product=intent.getExtras().getString("category");


//        lL=findViewById(R.id.product_title_layout);
        ImageView home=findViewById(R.id.product_spec_home);
        ImageView back=findViewById(R.id.product_spec_back);
        ImageView addToCart=findViewById(R.id.product_spec_cart_view);

        TextView productType=findViewById(R.id.product_name_type);
        productType.setText(product);
        TextView view=findViewById(R.id.product_name);
        view.setText(product);
        listView=findViewById(R.id.product_type_list);

/*        Button rating=findViewById(R.id.prod_rate);
        Button type=findViewById(R.id.prod_type);
        Button rateSubmit=findViewById(R.id.rate_submit);
        Button typeSubmit=findViewById(R.id.type_submit);
        final LinearLayout linearLayoutOne=findViewById(R.id.linear_one_spinner_layout);
        final LinearLayout linearLayoutTwo=findViewById(R.id.linear_two_spinner_layout);
        voltagespin = (MaterialSpinner) findViewById(R.id.voltage_rating_spin);
        ratingspin = (MaterialSpinner) findViewById(R.id.rating_rating_spin);
        typespin = (MaterialSpinner) findViewById(R.id.typespinner);*/

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProductSpecActivity.this,Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProductSpecActivity.this,OrderActivity.class));
            }
        });

        checkInternet(product);

       /* if (isRating && isType) {
                    rating.setVisibility(View.VISIBLE);
                    type.setVisibility(View.VISIBLE);
        } else if (isRating && !isType) {
                    rating.setVisibility(View.VISIBLE);
                    type.setVisibility(View.GONE);
        } else if (isType && !isRating) {
                    rating.setVisibility(View.GONE);
                    type.setVisibility(View.VISIBLE);
        } else {
                    rating.setVisibility(View.GONE);
                    type.setVisibility(View.GONE);
        }*/

       /* type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionManager.beginDelayedTransition(lL);
                }
                visible=!visible;
                visibles=visible;
                linearLayoutTwo.setVisibility(visible? View.VISIBLE:View.GONE);
                int k=linearLayoutOne.getVisibility();
                if(k==View.VISIBLE)
                {
                    linearLayoutOne.setVisibility(View.GONE);
                }

                if(visible) {
                    NetworkConnection networkConnection = new NetworkConnection(ProductSpecActivity.this);
                    if (networkConnection.CheckInternet())
                        typeMethod(product);
                    else {
                        StyleableToasty styleableToasty = new StyleableToasty(ProductSpecActivity.this);
                        styleableToasty.showSuccessToast("Please Check Your Internet Connection");
                    }
                }
            }
        });


        rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionManager.beginDelayedTransition(lL);
                }
                visibles=!visibles;
                visible=visibles;
                linearLayoutOne.setVisibility(visibles ? View.VISIBLE : View.GONE);
                int k=linearLayoutTwo.getVisibility();
                if(k==View.VISIBLE)
                {
                    linearLayoutTwo.setVisibility(View.GONE);
                }

                if(visibles)
                {
                    NetworkConnection networkConnection = new NetworkConnection(ProductSpecActivity.this);
                    if (networkConnection.CheckInternet())
                        setSpin(product);
                    else {
                        StyleableToasty styleableToasty = new StyleableToasty(ProductSpecActivity.this);
                        styleableToasty.showSuccessToast("Please Check Your Internet Connection");
                    }
                }
            }
        });

        voltagespin.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                String items=item.toString();
                if(!items.equals("Please select voltage"))
                {
                    NetworkConnection networkConnection=new NetworkConnection(ProductSpecActivity.this);
                    if(networkConnection.CheckInternet())
                         ratingMethod(product,items);
                    else
                    {
                        StyleableToasty styleableToasty=new StyleableToasty(ProductSpecActivity.this);
                        styleableToasty.showSuccessToast("Please Check Your Internet Connection");
                    }
                }
                else {
                    ratingspinlist.clear();
                    ratingspinlist.add(0, "Please select rating");
                    ratingspin.setItems(ratingspinlist);
                    ratingspin.setPadding(30, 0, 0, 0);
                }

            }
        });

        typeSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Rating = "";
                String Voltage = "";
                String Type = typespin.getText().toString().trim();
                if (Type.equals("Please select type")) {
                    Type = "";
                    Toast.makeText(ProductSpecActivity.this, "Please select type", Toast.LENGTH_LONG).show();
                } else {
//                    checkInternet(Rating,Voltage,Type);
                    startActivity(new Intent(ProductSpecActivity.this,ProductListView.class)
                            .putExtra("Product",product)
                            .putExtra("Rating","")
                            .putExtra("Voltage","")
                            .putExtra("Type",Type));
                }
            }
        });

        rateSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Rating = ratingspin.getText().toString().trim();
                String Voltage = voltagespin.getText().toString().trim();

                if (Voltage.equals("Please select voltage")) {
                    Voltage = "";
                    Toast.makeText(ProductSpecActivity.this, "Please select voltage", Toast.LENGTH_SHORT).show();
                } else if (Rating.equals("Please select rating")) {
                    Rating = "";
                    Toast.makeText(ProductSpecActivity.this, "Please select rating", Toast.LENGTH_SHORT).show();
                } else {
                    String Type = "";
//                    checkInternet(Rating,Voltage,Type);
                    startActivity(new Intent(ProductSpecActivity.this,ProductListView.class)
                            .putExtra("Product",product)
                            .putExtra("Rating",Rating)
                            .putExtra("Voltage",Voltage)
                            .putExtra("Type",""));
                }
            }
        });
*/
    }

    private void ratingMethod(String product,String selectItem)
    {
        loading = ProgressDialog.show(ProductSpecActivity.this, "Loading Rating", "Please wait", false, false);
        try {
            CategoryAPI service = RetroClient.getApiService();

//            Call<RatingList> call = service.ratingList(product, voltagespin.getText().toString().trim());
            Call<RatingList> call = service.ratingList(product, selectItem);
            call.enqueue(new Callback<RatingList>() {
                @Override
                public void onResponse(Call<RatingList> call, Response<RatingList> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("success")) {
                        ratingspinlist = new ArrayList<String>();
                        ratingspinlist = response.body().getData();
                        ratingspinlist.add(0, "Please select rating");
                        ratingspin.setItems(ratingspinlist);
//                        ratingspin.setPadding(30, 0, 0, 0);
//                                            Rating = ratingspin.getText().toString().trim();

                    } else if (response.body().getResult().equals("notsuccess")){

                        Toast.makeText(ProductSpecActivity.this, "No Data Found", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        ratingspinlist.clear();
                        ratingspinlist.add(0, "Please select rating");
                        ratingspin.setItems(ratingspinlist);
//                        ratingspin.setPadding(30, 0, 0, 0);
//                                            Rating = ratingspin.getText().toString().trim();
                        Toast.makeText(ProductSpecActivity.this, "No Ratings Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<RatingList> call, Throwable t) {
                    loading.dismiss();
                    Toast.makeText(ProductSpecActivity.this, "No Ratings Available", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void setSpin(String product)
    {
        loading = ProgressDialog.show(ProductSpecActivity.this, "Loading Voltage", "Please wait", false, false);
        try {

            CategoryAPI service = RetroClient.getApiService();
            Call<VoltageList> call = service.voltageList(product);

            call.enqueue(new Callback<VoltageList>() {
                @Override
                public void onResponse(Call<VoltageList> call, Response<VoltageList> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("success")) {
                        List<String> voltagespinlist = response.body().getData();
                        voltagespinlist.add(0, "Please select voltage");
                        voltagespin.setItems(voltagespinlist);
//                        voltagespin.setPadding(30, 0, 0, 0);
                    } else if (response.body().getResult().equals("notsuccess"))
                    {
                        Toast.makeText(ProductSpecActivity.this, "No Data Found", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ProductSpecActivity.this, "No Voltage Available", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<VoltageList> call, Throwable t) {
                    loading.dismiss();
                    Toast.makeText(ProductSpecActivity.this, "No Voltage Available", Toast.LENGTH_SHORT).show();

                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void checkInternet(String product)
    {
        final Alertbox alertbox=new Alertbox(ProductSpecActivity.this);
        NetworkConnection connection=new NetworkConnection(ProductSpecActivity.this);
        if(connection.CheckInternet())
        {
            searchType(product,alertbox);
        }
        else {
            alertbox.showNegativebox("No Internet Connection. Please try again");
        }
    }

    private void searchType(final String product, final Alertbox alertbox)
    {
        final ProgressDialog progressDialog = new ProgressDialog(ProductSpecActivity.this,
                R.style.Progress);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);

        progressDialog.show();
        CategoryAPI service = RetroClient.getApiService();
        Call<TypeList> call = service.typeList(product);
        call.enqueue(new Callback<TypeList>() {
            @Override
            public void onResponse(Call<TypeList> call, Response<TypeList> response) {
                try {
                    typespinlist = response.body().getData();
                    progressDialog.dismiss();
                    ProductTypeAdapter adapter = new ProductTypeAdapter(ProductSpecActivity.this, typespinlist, product);
                    listView.setAdapter(adapter);

                }catch (Exception e)
                {
                    progressDialog.dismiss();
                    alertbox.showNegativebox("Invalid Response. Please contact Admin");
                }
            }

            @Override
            public void onFailure(Call<TypeList> call, Throwable t) {
                progressDialog.dismiss();
                alertbox.showNegativebox("Invalid Search");
            }
        });
    }
    public void typeMethod(String product)
    {
        try {
            CategoryAPI service = RetroClient.getApiService();
            Call<TypeList> call = service.typeList(product);
            call.enqueue(new Callback<TypeList>() {
                @Override
                public void onResponse(Call<TypeList> call, Response<TypeList> response) {
                    if (response.body().getResult().equals("success")) {
                        typespinlist = response.body().getData();
                        typespinlist.add(0, "Please select type");
                        typespin.setItems(typespinlist);
                        typespin.setPadding(30, 0, 0, 0);
                    } else {
                        typespinlist.clear();
                        typespinlist.add(0, "Please select type");
                        typespin.setItems(typespinlist);
                        typespin.setPadding(30, 0, 0, 0);
                        Toast.makeText(ProductSpecActivity.this, "No Type Available", Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<TypeList> call, Throwable t) {
                    typespinlist.clear();
                    typespinlist.add(0, "Please select type");
                    typespin.setItems(typespinlist);
                    typespin.setPadding(30, 0, 0, 0);
                    Toast.makeText(ProductSpecActivity.this, "No Type Available", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
