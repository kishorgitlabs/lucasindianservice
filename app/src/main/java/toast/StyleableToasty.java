package toast;

import android.content.Context;
import android.graphics.Color;
import android.widget.Toast;

import com.brainmagic.lucasindianservice.demo.R;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

public class StyleableToasty {
    Context context;

    public StyleableToasty(Context context) {
        this.context = context;
    }

    public void showSuccessToast(String msg) {
        StyleableToast st = new StyleableToast(context,
                msg, Toast.LENGTH_SHORT);
        st.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        st.setTextColor(Color.WHITE);
        st.setMaxAlpha();
        st.show();


    }


    public void showFailureToast(String msg) {
        StyleableToast st = new StyleableToast(context,
                msg, Toast.LENGTH_SHORT);
        st.setBackgroundColor(context.getResources().getColor(R.color.green));
        st.setTextColor(Color.WHITE);
        st.setMaxAlpha();
        st.show();


    }

}
