
package api.models.scanhistory.totalpoints;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class GetRMCode {

    @SerializedName("data")
    private List<GetRMPointsList> mData;
    @SerializedName("result")
    private String mResult;

    public List<GetRMPointsList> getData() {
        return mData;
    }

    public void setData(List<GetRMPointsList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
