package com.brainmagic.lucasindianservice.demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class ProductConfigActivity extends AppCompatActivity {

    private boolean ratingBool,typeBool;
    private String category="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_product_config);

        LinearLayout ratingLayout=findViewById(R.id.rating_layout);
        LinearLayout typeLayout=findViewById(R.id.type_layout);

        ratingBool=getIntent().getBooleanExtra("rating",false);
        typeBool=getIntent().getBooleanExtra("type",false);
        category=getIntent().getStringExtra("category");





        if (ratingBool && typeBool) {
            ratingLayout.setVisibility(View.VISIBLE);
            typeLayout.setVisibility(View.VISIBLE);
        } else if (ratingBool && !typeBool) {
            ratingLayout.setVisibility(View.VISIBLE);
            typeLayout.setVisibility(View.GONE);
        } else if (typeBool && !ratingBool) {
            ratingLayout.setVisibility(View.GONE);
            typeLayout.setVisibility(View.VISIBLE);
        } else {
            ratingLayout.setVisibility(View.GONE);
            typeLayout.setVisibility(View.GONE);
        }
//
//        ratingLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(ProductSearch.this, ProductListView.class);
//                intent.putExtra("rating/type", "rating");
//                intent.putExtra("Product", catagoryList.get(i));
//                startActivity(intent);
//            }
//        });
//
//        typeLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(ProductSearch.this, ProductListView.class);
//                intent.putExtra("rating/type", "type");
//                intent.putExtra("Product", catagoryList.get(i));
//                startActivity(intent);
//            }
//        });

    }

    public void rating(View view)
    {
        Intent intent = new Intent(ProductConfigActivity.this, ProductListView.class);
        intent.putExtra("rating/type", "rating");
        intent.putExtra("Product", category);
        startActivity(intent);
//        finish();
    }

    public void type(View view)
    {
        Intent intent = new Intent(ProductConfigActivity.this, ProductListView.class);
        intent.putExtra("rating/type", "type");
        intent.putExtra("Product", category);
        startActivity(intent);
    }
}
