package com.brainmagic.lucasindianservice.demo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.KeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import ApiInterface.CategoryAPI;
import LocalDB.PartDetailsDB;
import RetroClient.RetroClient;
import adapter.OrderAdapter;
import adapter.ViewaddressAdapter;
import alertbox.Alertbox;
import api.models.commen.CommenList;
import api.models.getpreviousorder.GetPreviousAddress;

import api.models.scan.validateOTP.ValidateOTP;
import api.retrofit.APIService;
import home.Home_Activity;
import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import model.GetUserDetails.GetUserDetails;
import model.GetUserDetails.GetUserDetailsList;
import model.Order_Parts;
import model.Order_Parts;
import model.PartModelClass;
import model.PlaceOrder;
import model.deliverydetails.Order;
import model.ordermodel.OrderModel;
import network.NetworkConnection;
import orders.QuickOrders;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;


public class OrderActivity extends AppCompatActivity {
    private ArrayList<Order_Parts> order_Parts;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private OrderAdapter orderAdapter;
//    private AlertDialog alertDialog;
    private AlertDialog address;
    private AlertDialog viewaddressalert;
    private AlertDialog orderplaced;
    private PartDetailsDB db;
    private String code="";
    private boolean checkOthers=false;
    String newaddress;
    private AlertDialog alertDialog;
    private String userAddress;
    private List<GetUserDetailsList> userDetailsLists;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        ImageView back=findViewById(R.id.order_back);
        ImageView home=findViewById(R.id.order_home);
        TextView placeOrder=findViewById(R.id.place_order);

        ListView list=findViewById(R.id.cart_order_list);

        //get part details
        db=new PartDetailsDB(OrderActivity.this);
        order_Parts=db.getCartItem();

        //pass the list to adapter
            orderAdapter=new OrderAdapter(OrderActivity.this,order_Parts);
        list.setAdapter(orderAdapter);

        //go back to previous activity
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        //go to home activity
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(OrderActivity.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });

        //to place order
        placeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                PartDetailsDB db=new PartDetailsDB(OrderActivity.this);
//                db.deleteTable();
//                getPartItems();
                //get cart items from local sqllite
                order_Parts=db.getCartItem();

                if(!order_Parts.isEmpty()) {
                    NetworkConnection networkConnection = new NetworkConnection(OrderActivity.this);
                    if (networkConnection.CheckInternet())
                        confirmOrder();
                    else {
                        StyleableToasty styleableToasty = new StyleableToasty(OrderActivity.this);
                        styleableToasty.showSuccessToast("Please check Your Internet Connection");
                    }
                }
                else {
                    new Alertbox(OrderActivity.this).showNegativebox("Cart list is empty");
                }
            }
        });
        //if order table is empty
        if(order_Parts.isEmpty())
        {
            StyleableToast st = new StyleableToast(OrderActivity.this,
                    "No Items In Your Cart", Toast.LENGTH_SHORT);
            st.setBackgroundColor(this.getResources().getColor(R.color.colorPrimary));
            st.setTextColor(Color.WHITE);
            st.setMaxAlpha();
            st.show();
//            Toast.makeText(getApplicationContext(),"No Items In Your Cart",Toast.LENGTH_SHORT).show();
            finish();
        }
    }


    //get the user previous address of the selected users
    private void getpreviousaddress(final List<String> addressnew, final EditText orderDeliveryAddress) {
        viewaddressalert=new AlertDialog.Builder(OrderActivity.this).create();
        View addaddress= LayoutInflater.from(OrderActivity.this).inflate(R.layout.viewaddressalert,null);
        ListView addresslist=addaddress.findViewById(R.id.addresslist);
        addresslist.setAdapter(new ViewaddressAdapter(viewaddressalert.getContext(),addressnew));

        //view the address list adapter
        addresslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                parent.getItemAtPosition(position);
                String selectedAddress=addressnew.get(position);
//               String selectedAddress= (String) parent.getSelectedItem();
                orderDeliveryAddress.setText(selectedAddress);
                viewaddressalert.dismiss();
            }
        });
        viewaddressalert.setView(addaddress);
        viewaddressalert.setCanceledOnTouchOutside(false);
        viewaddressalert.show();
    }

    //confirm the order
    public void confirmOrder()
    {
        alertDialog=new AlertDialog.Builder(this).create();
        final View view= LayoutInflater.from(OrderActivity.this).inflate(R.layout.confirm_order_dialog,null);
        myshare = getSharedPreferences("LIS", MODE_PRIVATE);
      //  final long dlr=Long.parseLong(myshare.getString("UserCode","1"));
        final String dlr=myshare.getString("UserCode","1");
        final String userType=myshare.getString("UserType","1");
        final String mobileNo=myshare.getString("Mobile","1");
        final String userName=myshare.getString("UserName","1");
         userAddress=myshare.getString("UserAddress","");


        final MaterialEditText orderName=view.findViewById(R.id.order_username);
        final EditText orderCode=view.findViewById(R.id.order_dlr_code);
        final MaterialEditText orderMobileNO =view.findViewById(R.id.order_mobile_no);
        final EditText orderDeliveryAddress =view.findViewById(R.id.order_delivery_address);
        final MaterialSpinner getUserTypes=view.findViewById(R.id.get_user_type);
        final Button confirmOrder=view.findViewById(R.id.confirm_Order);
        final TextView mailIdHead=view.findViewById(R.id.order_email_head);
        final TextView addnewaddress=view.findViewById(R.id.addnewaddress);
        final TextView viewaddress=view.findViewById(R.id.viewaddress);
        final MaterialEditText mailId=view.findViewById(R.id.order_mail_id);
        orderCode.setEnabled(false);

        final TextView orderNameHead=view.findViewById(R.id.order_name_head);
        final TextView orderCodeHead=view.findViewById(R.id.order_dlr_code_head);
        final TextView getUserTypesHead=view.findViewById(R.id.get_user_type_head);

        final SpinnerDialog spinnerDialog=new SpinnerDialog(OrderActivity.this,"Select Code");

        //list of user for lisso and msr to place order
        List<String> userTypeList=new ArrayList<>();
        userTypeList.add("Select User Type");
        userTypeList.add("Retailer");
        userTypeList.add("Jodidar");
        userTypeList.add("Stockist");

        getUserTypes.setItems(userTypeList);
        String tempUserType=userType;
        String otherTempUserType=userType;

        if(userType.equals("MSR")||userType.equals("LISSO"))
        {
            userTypeList.add("DLR");
            userTypeList.add("Others");
        }
        else if(userType.equals("DLR"))
        {
            userTypeList.add("LIS");
        }

        if(!tempUserType.equals("LISSO") && !userType.equals("MSR")&& !otherTempUserType.equals("DLR"))
        {
            orderName.setVisibility(View.GONE);
            orderCode.setVisibility(View.GONE);
            getUserTypes.setVisibility(View.GONE);
            orderNameHead.setVisibility(View.GONE);
            orderCodeHead.setVisibility(View.GONE);
            getUserTypesHead.setVisibility(View.GONE);

            orderMobileNO.setText(mobileNo);
//            orderMobileNO.setEnabled(false);
            orderMobileNO.setFocusable(false);

            if(!userType.equals("End User"))
            {
                orderDeliveryAddress.setText(userAddress);
//                orderDeliveryAddress.setEnabled(false);
                orderDeliveryAddress.setFocusable(false);
            }

        }

        //get the user type
        getUserTypes.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                code=item.toString();
                if(item.toString().equals("Others")|| item.toString().equals("LIS"))
                {
                    orderCode.setVisibility(View.GONE);
                    orderCodeHead.setVisibility(View.GONE);
                    mailIdHead.setVisibility(View.VISIBLE);
                    mailId.setVisibility(View.VISIBLE);
                    checkOthers=true;
//                    orderName.setKeyListener((KeyListener) orderName.getTag());
                    orderName.setEnabled(true);
                    orderMobileNO.setEnabled(true);
                    orderDeliveryAddress.setEnabled(true);
                    orderName.setText("");
                    orderMobileNO.setText("");
                    orderDeliveryAddress.setText("");

                }
                else if(!item.toString().equals("Select User Type"))
                {
                    orderCode.setText("");
                    orderName.setText("");
                    orderMobileNO.setText("");
                    orderDeliveryAddress.setText("");
                    orderCode.setEnabled(true);
                    getMechCodeAutocomplete(code,spinnerDialog);
                    orderCodeHead.setVisibility(View.VISIBLE);
                    orderCode.setVisibility(View.VISIBLE);
                    mailIdHead.setVisibility(View.GONE);
                    mailId.setVisibility(View.GONE);
                    checkOthers=false;
//                    orderName.setTag(orderName.getKeyListener());
//                    orderName.setKeyListener(null);
                    orderName.setEnabled(false);
                    orderMobileNO.setEnabled(false);
                    orderDeliveryAddress.setEnabled(false);
                }
                else {
                    orderCode.setText("");
                    orderName.setText("");
                    orderMobileNO.setText("");
                    orderDeliveryAddress.setText("");
                    orderCode.setEnabled(false);
                }
            }
        });

        //order code
        orderCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!code.equals("Select User Type"))
                {
                    try {
                        spinnerDialog.showSpinerDialog();
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        StyleableToasty styleableToasty=new StyleableToasty(OrderActivity.this);
                        styleableToasty.showSuccessToast("Error Occured");
                    }
                }
                else {
                    StyleableToasty styleableToasty=new StyleableToasty(OrderActivity.this);
                    styleableToasty.showSuccessToast("Select User type");
                }
            }
        });

        spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String s, int i) {
                orderCode.setText(s);
                orderName.setText(userDetailsLists.get(i).getName());
                orderMobileNO.setText(userDetailsLists.get(i).getMobileNo());
                orderDeliveryAddress.setText(userDetailsLists.get(i).getAddress());
            }
        });


        confirmOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NetworkConnection networkConnection=new NetworkConnection(OrderActivity.this);
                if(networkConnection.CheckInternet())
                {
                order_Parts=db.getCartItem();

                if(userType.equals("LISSO") || userType.equals("MSR")|| userType.equals("DLR"))
                {
                    String deliveryName=orderName.getText().toString();
                    String splitcode=orderCode.getText().toString();
                    String[] codes=splitcode.split(" ");
                    String orderedCode=codes[0];
                    String getUserType=getUserTypes.getText().toString();
                    String deliveryMobileNo=orderMobileNO.getText().toString();
                    String orderedDeliveryAddress=orderDeliveryAddress.getText().toString();
                    String mail=mailId.getText().toString();


                    if (getUserType.equals("Select User Type")) {
                        getUserTypes.setError("Select User Type");
                    }
                    if(code.equals("Jodidar")||code.equals("Retailer")||code.equals("Stockist")) {
                        if (getUserType.equals("Select User Type")) {
                            getUserTypes.setError("Select User Type");
                        } else if (TextUtils.isEmpty(orderedCode) && !checkOthers) {
                            orderCode.setError("User Code cannot be Empty");
                        } else if (TextUtils.isEmpty(deliveryName)) {
                            orderName.setError("Order Name cannot be Empty");
                        } else if (TextUtils.isEmpty(deliveryMobileNo)) {
                            orderMobileNO.setError("Delivery mobile no cannot be Empty");
                        } else if (TextUtils.isEmpty(orderedDeliveryAddress)) {
                            orderDeliveryAddress.setError("Delivery Address no cannot be Empty");
                        }
                       else {

                           //set the order data to the order model to pass as a list in json format
                            Order order = new Order();
                            // Long orderDlrCode=Long.parseLong(orderedCode);
                            order.setDLRCode(orderedCode);
                            order.setmDistributorCode(dlr);
//                order.setDistributorid(t);
                            order.setDeliveryMobile(deliveryMobileNo);
                            order.setDeliveryAddress(orderedDeliveryAddress);
                            order.setDeliveryName(deliveryName);
                            order.setOrderedByName(userName);
                            order.setOrderedMobile(mobileNo);
                            order.setDeliveryStatus("Pending");
                            order.setOrderStatus("New");
                            order.setUsertype(getUserType);
                            order.setmDistributorusertype(userType);
                            order.setActive("Y");
                            getPartItems(order);
                        }
                    }
                    else if (code.equals("Others") || code.equals("LIS")) {

                        if (TextUtils.isEmpty(deliveryName)) {
                            orderName.setError("Order Name cannot be Empty");
                        } else if (TextUtils.isEmpty(deliveryMobileNo)) {
                            orderMobileNO.setError("Delivery mobile no cannot be Empty");
                        }   else if (TextUtils.isEmpty(mail)) {
                            mailId.setError("Mail Id cannot be Empty");
                        } else if (TextUtils.isEmpty(orderedDeliveryAddress)) {
                            orderDeliveryAddress.setError("Delivery Address no cannot be Empty");
                        }
                        else {

                            //set the order data to the order model to pass as a list in json format
                            Order order = new Order();
                            // Long orderDlrCode=Long.parseLong(orderedCode);
                            order.setDLRCode(orderedCode);
                            order.setmDistributorCode(dlr);
//                order.setDistributorid(t);
                            order.setDeliveryMobile(deliveryMobileNo);
                            order.setDeliveryAddress(orderedDeliveryAddress);
                            order.setDeliveryName(deliveryName);
                            order.setOrderedByName(userName);
                            order.setOrderedMobile(mobileNo);
                            order.setDeliveryStatus("Pending");
                            order.setOrderStatus("New");
                            order.setUsertype(getUserType);
                            order.setmDistributorusertype(userType);
                            order.setActive("Y");
                            getPartItems(order);
                        }

                    }
                }
                else {

                    if(userType.equals("End User"))
                    {
                        String orderedDeliveryAddress=orderDeliveryAddress.getText().toString();
                        if(TextUtils.isEmpty(orderedDeliveryAddress))
                        {
                            StyleableToasty styleableToasty=new StyleableToasty(OrderActivity.this);
                            styleableToasty.showSuccessToast("Enter Delivery Address");
                        }
                        else {
                            //set the order data to the order model to pass as a list in json format for dlr
                                        Order order =new Order();
                                        order.setDLRCode(dlr);
//                                        order.setmDistributorCode(dlr);
//                order.setDistributorid(t);
                                        order.setDeliveryMobile(mobileNo);
                                        order.setDeliveryAddress(orderedDeliveryAddress);
                                        order.setDeliveryName(userName);
                                        order.setOrderedByName(userName);
                                        order.setOrderedMobile(mobileNo);
                                        order.setDeliveryStatus("Pending");
                                        order.setOrderStatus("New");
                                        order.setUsertype(userType);
                                        order.setmDistributorusertype("");
                                        order.setActive("Y");
                                        getPartItems(order);
                        }
                    }
                    else {
                        //set the order data to the order model to pass as a list in json format
                        Order order =new Order();
                        order.setDLRCode(dlr);
//                order.setmDistributorCode(dlr);
//                order.setDistributorid(t);
                        order.setDeliveryMobile(mobileNo);
                        order.setDeliveryAddress(userAddress);
                        order.setDeliveryName(userName);
                        order.setOrderedByName(userName);
                        order.setOrderedMobile(mobileNo);
                        order.setDeliveryStatus("Pending");
                        order.setOrderStatus("New");
                        order.setUsertype(userType);
                        order.setmDistributorusertype("");
                        order.setActive("Y");
                        getPartItems(order);
                    }
//                    deliveryMobileNo=orderMobileNO.getText().toString();
//                    orderedDeliveryAddress=orderDeliveryAddress.getText().toString();
//                    if(TextUtils.isEmpty(deliveryMobileNo)&&TextUtils.isEmpty(orderedDeliveryAddress))
//                    {
//                        MaterialDialog materialDialog = new MaterialDialog.Builder(OrderActivity.this)
//                                .title("Alert")
//                                .content("Your Default Mobile number and Address will be taken as your Delivery Number and Address")
//                                .positiveText("OK")
//                                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                                    @Override
//                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                        deliveryMobileNo = mobileNo;
//                                        orderedDeliveryAddress = userAddress;
//
//                                        Order order =new Order();
//                                        order.setDLRCode(dlr);
////                                        order.setmDistributorCode(dlr);
////                order.setDistributorid(t);
//                                        order.setDeliveryMobile(deliveryMobileNo);
//                                        order.setDeliveryAddress(orderedDeliveryAddress);
//                                        order.setDeliveryName(userName);
//                                        order.setOrderedByName(userName);
//                                        order.setOrderedMobile(mobileNo);
//                                        order.setDeliveryStatus("Pending");
//                                        order.setOrderStatus("New");
//                                        order.setUsertype(userType);
//                                        order.setmDistributorusertype("");
//                                        order.setActive("Y");
//                                        getPartItems(order);
//
////                                        Order order =new Order();
////                                        order.setDLRCode("");
////                                        order.setmDistributorCode("");
//////                order.setDistributorid(t);
////                                        order.setDeliveryMobile("");
////                                        order.setDeliveryAddress("");
////                                        order.setDeliveryName("");
////                                        order.setOrderedByName("");
////                                        order.setOrderedMobile("");
////                                        order.setDeliveryStatus("Pending");
////                                        order.setOrderStatus("New");
////                                        order.setUsertype("");
////                                        order.setmDistributorusertype("");
////                                        order.setActive("Y");
////                                        getPartItems(order);
//                                    }
//                                })
//                                .negativeText("Cancel")
//                                .onNegative(new MaterialDialog.SingleButtonCallback() {
//                                    @Override
//                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                        dialog.dismiss();
//                                    }
//                                }).show();
//
//                    }
//                    else {
//                        if (TextUtils.isEmpty(deliveryMobileNo)) {
//                            MaterialDialog materialDialog = new MaterialDialog.Builder(OrderActivity.this)
//                                    .title("Alert")
//                                    .content("Your Default Mobile number will be taken as your Delivery Number")
//                                    .positiveText("OK")
//                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
//                                        @Override
//                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                            deliveryMobileNo = mobileNo;
//                                            Order order =new Order();
//                                            order.setDLRCode(dlr);
//                                            order.setmDistributorCode(dlr);
////                order.setDistributorid(t);
//                                            order.setDeliveryMobile(deliveryMobileNo);
//                                            order.setDeliveryAddress(orderedDeliveryAddress);
//                                            order.setDeliveryName(userName);
//                                            order.setOrderedByName(userName);
//                                            order.setOrderedMobile(mobileNo);
//                                            order.setDeliveryStatus("Pending");
//                                            order.setOrderStatus("New");
//                                            order.setUsertype(userType);
//                                            order.setmDistributorusertype("");
//                                            order.setActive("Y");
//                                            getPartItems(order);
//                                        }
//                                    })
//                                    .negativeText("Cancel")
//                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
//                                        @Override
//                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                            dialog.dismiss();
//                                        }
//                                    }).show();
//
//                        } else if (TextUtils.isEmpty(orderedDeliveryAddress)) {
//
//                            MaterialDialog materialDialog = new MaterialDialog.Builder(OrderActivity.this)
//                                    .title("Alert")
//                                    .content("Your Default Address will be taken as your Delivery Address")
//                                    .positiveText("OK")
//                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
//                                        @Override
//                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                            orderedDeliveryAddress = userAddress;
//                                            Order order =new Order();
//                                            order.setDLRCode(dlr);
//                                            order.setmDistributorCode(dlr);
////                order.setDistributorid(t);
//                                            order.setDeliveryMobile(deliveryMobileNo);
//                                            order.setDeliveryAddress(orderedDeliveryAddress);
//                                            order.setDeliveryName(userName);
//                                            order.setOrderedByName(userName);
//                                            order.setOrderedMobile(mobileNo);
//                                            order.setDeliveryStatus("Pending");
//                                            order.setOrderStatus("New");
//                                            order.setUsertype(userType);
//                                            order.setmDistributorusertype("");
//                                            order.setActive("Y");
//                                            getPartItems(order);
//                                        }
//                                    })
//                                    .negativeText("Cancel")
//                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
//                                        @Override
//                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                            dialog.dismiss();
//                                        }
//                                    }).show();
//                        }
//                        else {
//                            String deliveryMobileNo=orderMobileNO.getText().toString();
//                            String orderedDeliveryAddress=orderDeliveryAddress.getText().toString();
//
//                            Order order =new Order();
//                            order.setDLRCode(dlr);
//                            order.setmDistributorCode(dlr);
////                order.setDistributorid(t);
//                            order.setDeliveryMobile(deliveryMobileNo);
//                            order.setDeliveryAddress(orderedDeliveryAddress);
//                            order.setDeliveryName(userName);
//                            order.setOrderedByName(userName);
//                            order.setOrderedMobile(mobileNo);
//                            order.setDeliveryStatus("Pending");
//                            order.setOrderStatus("New");
//                            order.setUsertype(userType);
//                            order.setmDistributorusertype("");
//                            order.setActive("Y");
//                            getPartItems(order);
//                        }
//                    }
                }
                }
                else {
                    StyleableToasty styleableToasty=new StyleableToasty(OrderActivity.this);
                    styleableToasty.showSuccessToast("Please check Your Internet Connection");
                }
            }
        }
        );

        //view address
        viewaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String orderedCode;
                String getUserType;

                //get the user type wise pass data
                if(userType.equals("LISSO") || userType.equals("MSR")|| userType.equals("DLR")) {
                    String splitcode = orderCode.getText().toString();
                    String[] codes = splitcode.split(" ");
                    orderedCode = codes[0];
                    getUserType=getUserTypes.getText().toString();
                }else {

                    orderedCode=myshare.getString("UserCode","1");
                    getUserType=myshare.getString("UserType","1");
                }

                try {
                    final ProgressDialog loading = ProgressDialog.show(viewaddress.getContext(), getString(R.string.app_name), "Loading...", false, false);
                    CategoryAPI service=RetroClient.getApiService();
                    Call<GetPreviousAddress> call = service.getaddress(orderedCode,getUserType);
                    call.enqueue(new Callback<GetPreviousAddress>() {
                        @Override
                        public void onResponse(Call<GetPreviousAddress> call, Response<GetPreviousAddress> response) {
                            try {

                                if (response.body().getResult().equals("Success")) {
                                    loading.dismiss();

                                    List<String> addressnew= new ArrayList<>(new HashSet<>(response.body().getData()));
                                    getpreviousaddress(addressnew,orderDeliveryAddress);
                                }
                                else if(response.body().getResult().equals("NotSuccess"))
                                {
                                    loading.dismiss();
                                    StyleableToasty styleableToasty=new StyleableToasty(OrderActivity.this);
                                    styleableToasty.showSuccessToast("You Did Not Have Previous Address");
                                }
                                else {
                                    loading.dismiss();
                                    StyleableToasty styleableToasty=new StyleableToasty(OrderActivity.this);
                                    styleableToasty.showSuccessToast("You Did Not Have Previous Address");

//                            alertbox.showNegativebox(getString(R.string.server_error));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                loading.dismiss();
                                StyleableToasty styleableToasty=new StyleableToasty(OrderActivity.this);
                                styleableToasty.showSuccessToast("You Did Not Have Previous Address");

                            }
                        }

                        @Override
                        public void onFailure(Call<GetPreviousAddress> call, Throwable t) {
                            loading.dismiss();
                            t.printStackTrace();
                            StyleableToasty styleableToasty=new StyleableToasty(OrderActivity.this);
                            styleableToasty.showSuccessToast("You Did Not Have Previous Address");

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    StyleableToasty styleableToasty=new StyleableToasty(OrderActivity.this);
                    styleableToasty.showSuccessToast("You Did Not Have Previous Address");
                }

            }



        });



        //add new address
        addnewaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getnewaddress();

            }


        //getnewaddress
            private void getnewaddress() {
                address=new AlertDialog.Builder(OrderActivity.this).create();
                View addaddress= LayoutInflater.from(OrderActivity.this).inflate(R.layout.addnewaddressalert,null);
                final EditText newaddresstext=addaddress.findViewById(R.id.newaddresstext);
                TextView submitnewaddress=addaddress.findViewById(R.id.submitnewaddress);


                submitnewaddress.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        newaddress=newaddresstext.getText().toString();
                        if (newaddress.equals("")){

                            newaddresstext.setError("New Address cannot be Empty");
                        }
                        else{
                            orderDeliveryAddress.setText(newaddress);
                            address.dismiss();
                        }

//                        newaddress=myshare.getString("UserAddress","");
                    }
                });

                address.setView(addaddress);
                address.show();
            }
        });
        alertDialog.setView(view);
        alertDialog.show();
    }
    //getmechautocomplete
    private void getMechCodeAutocomplete(final String mSelectedUsertype, final SpinnerDialog spinnerDialog) {
        final Alertbox alertbox=new Alertbox(OrderActivity.this);

        try {
            final ProgressDialog loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
//            APIService service = api.retrofit.RetroClient.getApiService();

            CategoryAPI service=RetroClient.getApiService();

            Call<GetUserDetails> call = service.getUserDetails(mSelectedUsertype);


            call.enqueue(new Callback<GetUserDetails>() {
                @Override
                public void onResponse(Call<GetUserDetails> call, Response<GetUserDetails> response) {
                    try {

                        if (response.body().getResult().equals("Success")) {
//                            mJm_codeList = (ArrayList<String>) response.body().getData();
//                            OnCodesSuccess(mJm_codeList);
                            List<String> code=new ArrayList<>();
                            userDetailsLists=response.body().getData();
                            //Loop is not recommended if problems occurs should change it by passing values of user type and user code
                            for(GetUserDetailsList userCode:response.body().getData())
                            {
                                code.add(userCode.getDLRCode()+"  "+userCode.getName());
                            }
                            loading.dismiss();
                            spinnerDialog.SetItems((ArrayList<String>)code);
                        }
                        else if(response.body().getResult().equals("NotSuccess"))
                        {
                            loading.dismiss();
                            alertbox.showAlertbox("No "+mSelectedUsertype +" code found ");
                        }

                        else {
                            loading.dismiss();
                            alertbox.showNegativebox("No "+mSelectedUsertype +" code found ");
//                            alertbox.showNegativebox(getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showNegativebox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<GetUserDetails> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showNegativebox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //getpartdetails
    public void getPartItems(final Order order)
    {
        long overallAmount=0;
        String size=String.valueOf(order_Parts.size());
        for(int i=0;i<order_Parts.size();i++)
        {
            String amt=order_Parts.get(i).getPartPrice();
            if (amt==null){
                alertDialog.dismiss();
                StyleableToast st = new StyleableToast(this,
                        "Mrp Price For This Product Did not Updated Till Now Please Contact LIS Admin", Toast.LENGTH_LONG);
                st.setBackgroundColor(this.getResources().getColor(R.color.colorPrimary));
                st.setTextColor(Color.WHITE);
                st.setMaxAlpha();
                st.show();
            }
            else {
                String[] split = amt.split("\\.");
                int mrp = Integer.parseInt(split[0]);
                int quantity = Integer.parseInt(order_Parts.get(i).getPartQuantity());
//            int amount=Integer.parseInt(order_Parts.get(i).getPartPrice());
                String totalAmount = String.valueOf(quantity * mrp);
                order_Parts.get(i).setTotalAmount(totalAmount);
                order_Parts.get(i).setActive("Y");

                int overallAmountTemp = Integer.parseInt(order_Parts.get(i).getTotalAmount());
                overallAmount += overallAmountTemp;
            }
        }
//        myshare = getSharedPreferences("LIS", MODE_PRIVATE);
//        String dlr=(myshare.getString("UserCode","1"));
//        String userType=myshare.getString("UserType","1");
//        String mobileNo=myshare.getString("Mobile","1");
//        String userName=myshare.getString("UserName","1");

        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String Date = dateFormat.format(c.getTime());


        order.setTotalOrderAmount(overallAmount);
        order.setOrderDate(Date);
        order.setTotalOrderedQty(size);

//        for(int i=0;i<order_Parts.size();i++)
//        {
//            int quantity=Integer.parseInt(order_Parts.get(i).getPartQuantity());
//            int amount=Integer.parseInt(order_Parts.get(i).getPartPrice());
//            String totalAmount=String.valueOf(quantity*amount);
//            order_Parts.get(i).setTotalAmount(totalAmount);
//            order_Parts.get(i).setActive("Y");
//            order_Parts.get(i).setPartDescription("");
//            order_Parts.get(i).setPartName("");
//        }


        final OrderModel orderModel=new OrderModel();
        orderModel.setCartModelDetails(order_Parts);
        orderModel.setOrder(order);
        final ProgressDialog progressDialog = new ProgressDialog(OrderActivity.this,
            R.style.Progress);
        try {
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);

            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();

            Call<PlaceOrder> call = service.placeOrder(orderModel);

            call.enqueue(new Callback<PlaceOrder>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<PlaceOrder> call, Response<PlaceOrder> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {

                        getorderstatus(response.body().getData());
//                        alertDialog.dismiss();

//                        new MaterialDialog.Builder(OrderActivity.this)
//                                .title("Order")
//                                .content("Your Order Number is " + response.body().getData())
//                                .positiveText("Okay")
//                                .canceledOnTouchOutside(false)
//                                .positiveColor(getResources().getColor(R.color.colorPrimary))
//                                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                                    @Override
//                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//
//                                        dialog.dismiss();
//                                        onBackPressed();
////                                        alertDialog.dismiss();
//                                    }
//                                })
//
//                                .negativeText("More Shopping")
//                                .canceledOnTouchOutside(false)
//                                .onNegative(new MaterialDialog.SingleButtonCallback() {
//                                    @Override
//                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                        Intent moreshopping = new Intent(getApplicationContext(),ProductCatalogue.class);
//                                        startActivity(moreshopping);
//                                    }
//                                })
//                                .show();

//                        Toast.makeText(getApplicationContext(), "Your Order number is " + response.body().getData(), Toast.LENGTH_SHORT).show();
                        PartDetailsDB db=new PartDetailsDB(OrderActivity.this);
                        db.deleteTable();

                    }
                    else if(response.body().getResult().equals("NotFound"))
                    {
                        final Alertbox alertbox=new Alertbox(OrderActivity.this);
                        alertbox.showAlertbox("Order Can not be Placed for Mr. "+order.getDeliveryName()+" due to invalid data. Please Contact Admin");
                        alertDialog.dismiss();
                    }
                    else {
                        alertDialog.dismiss();
                        StyleableToasty styleableToasty=new StyleableToasty(OrderActivity.this);
                        styleableToasty.showSuccessToast("Something went Wrong. Please try again later");
//                        Toast.makeText(getApplicationContext(), "Please contact Admin", Toast.LENGTH_SHORT).show();
                    }
                }
                //get order status
                private void getorderstatus(String data) {
                    orderplaced=new AlertDialog.Builder(OrderActivity.this).create();
                    View orderplacedshow= LayoutInflater.from(OrderActivity.this).inflate(R.layout.orderplaced,null);

                    TextView ordernumber=orderplacedshow.findViewById(R.id.ordernumber);
                    TextView moreshopping=orderplacedshow.findViewById(R.id.moreshopping);
                    TextView okcart=orderplacedshow.findViewById(R.id.okcart);

                    String ordernumbergenerated=data;
                    ordernumber.setText("Your Order Number is " + ordernumbergenerated);

                    okcart.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent home=new Intent(getApplicationContext(),Home_Activity.class);
                            startActivity(home);
                        }

                    });

                    moreshopping.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent moreshopping=new Intent(getApplicationContext(),ProductCatalogue.class);;
                            startActivity(moreshopping);
                        }
                    });
                    orderplaced.setView(orderplacedshow);
                    orderplaced.setCanceledOnTouchOutside(false);
                    orderplaced.show();
                }

                @Override
                public void onFailure(Call<PlaceOrder> call, Throwable t) {
                    progressDialog.dismiss();
                    StyleableToasty styleableToasty=new StyleableToasty(OrderActivity.this);
                    styleableToasty.showSuccessToast("Failed to Add the items. Please try again later");
//                    Toast.makeText(getApplicationContext(), "error ", Toast.LENGTH_SHORT).show();
                }
            });

        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
//            Toast.makeText(getApplicationContext(), "Exception ", Toast.LENGTH_SHORT).show();
            StyleableToasty styleableToasty=new StyleableToasty(OrderActivity.this);
            styleableToasty.showSuccessToast("Error Occurred");
        }
    }

}
