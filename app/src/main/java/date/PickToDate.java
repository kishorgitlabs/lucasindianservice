package date;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.widget.DatePicker;

import java.util.Calendar;

public class PickToDate extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public SendToDate sD;
    private String fromDate="";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Calendar c = Calendar.getInstance();

        Bundle bundle=getArguments();
        if(!bundle.isEmpty()) {
            fromDate = bundle.getString("fromDate").trim();
        }
        if(!TextUtils.isEmpty(fromDate) ) {

            int year = c.get(Calendar.YEAR);

            int month = c.get(Calendar.MONTH);

            int day = c.get(Calendar.DAY_OF_MONTH);

            final Calendar cl = Calendar.getInstance();
            final Calendar c2 = Calendar.getInstance();
            String[] dates=fromDate.split("-");
            int years= Integer.parseInt(dates[0]);
            int months= Integer.parseInt(dates[1]);
            int days= Integer.parseInt(dates[2]);
            cl.set(years,months-1,days+90);


            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, years, months, days);

            datePickerDialog.getDatePicker().setMaxDate(cl.getTimeInMillis());
//            Long d = 7776000000L;//90 days in millis
            c2.set(years,months-1,days+1);
            datePickerDialog.getDatePicker().setMinDate(c2.getTimeInMillis());


//            datePickerDialog.getDatePicker().setMaxDate(90);


            // Create a new instance of DatePickerDialog and return it

            return datePickerDialog;
        }
        return null;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        String mToDate = year + "-" + (month + 1) + "-" + day;
        sD.sendToDate(mToDate);
    }


    public interface SendToDate {
        void sendToDate(String msg);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            sD = (SendToDate) getActivity();
        } catch (Exception e) {

        }
    }

    public void setFromDate(String fromDate)
    {
        this.fromDate=fromDate;
    }
}
