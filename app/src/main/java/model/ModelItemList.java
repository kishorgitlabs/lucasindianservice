
package model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ModelItemList {

    @SerializedName("image")
    private String mImage;
    @SerializedName("model")
    private String mModel;

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getModel() {
        return mModel;
    }

    public void setModel(String model) {
        mModel = model;
    }

}
