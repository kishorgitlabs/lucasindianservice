package ApiInterface;

import api.models.gifthistory.GetGiftHistory;
import model.brandproductlist.GetBrandWiseProduct;
import model.getbrand.GetBrandList;
import api.models.getpreviousorder.GetPreviousAddress;
import api.models.overallsearch.OverallSearchResult;
import model.AppPart;
import model.FullUnitNoList;
import model.FullUnitSearch;
import model.GetUserDetails.GetUserDetails;
import model.ModelList;
import model.OECustomers;
import model.PartNoList;
import model.PartNoSearch;
import model.PartSearch.ChildParts;
import model.PartSearch.PartSearch;
import model.PartSearch.ServiceParts;
import model.PlaceOrder;
import model.ProductSearch;
import model.RatingList;
import model.SearchFullUnit;
import model.TypeList;
import model.Universal.UniversalList;
import model.VoltageList;
import model.getbranddetails.BrandDetails;
import model.orderhistory.CompletedOrders;
import model.orderhistory.PendingCompletedOrders;
import model.orderhistory.PendingOrders;
import model.ordermodel.OrderModel;
import model.partnumberforbrand.PartNumberBrandWise;
import model.productsearch.ProductType;
import model.productsearch.ProductView;
import model.segment.SegmentSearch;
import model.uploadimage.UploadImage;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface CategoryAPI {

    @GET("Partnolist")
    Call<PartNoList> partNumberlist();

    @GET("Partnoorderlist")
    Call<PartNoList> childServiceList();

    @GET("Prodlist1")
    Call<ProductView> partList();

    @GET("segmentSearch")
    Call<SegmentSearch> segmentList();

    @FormUrlEncoded
    @POST("Productwisetypevolt")
    Call<ProductType> productType(
            @Field("Productname") String Productname
    );

    @FormUrlEncoded
    @POST("Partsearch")
    Call<PartNoSearch> partNoSearch(
            @Field("PartNumber") String PartNumber
    );

    @GET("FullUnitPartnolist")
    Call<FullUnitNoList> fullUnitList();

    @FormUrlEncoded
    @POST("FullunitPartsearch")
    Call<FullUnitSearch> fullUnitSearch(
            @Field("PartNumber") String PartNumber
    );

    @FormUrlEncoded
    @POST("FullunitPartsearch")
    Call<SearchFullUnit> searchFullUnit(
            @Field("PartNumber") String PartNumber
    );

    @FormUrlEncoded
    @POST("PartVolt")
    Call<VoltageList> voltageList(
            @Field("Productname") String Productname
    );

    @FormUrlEncoded
    @POST("PartOutput")
    Call<RatingList> ratingList(
    @Field("Productname") String Productname,
    @Field("Part_Volt") String Part_Volt);

    @FormUrlEncoded
    @POST("PartType")
    Call<TypeList> typeList(
            @Field("Productname") String Productname);

    @FormUrlEncoded
    @POST("Productsearch")
    Call<ProductSearch> productSearch(
            @Field("Productname") String Productname,
            @Field("Part_Outputrng") String Part_Outputrng,
            @Field("Pro_type") String Pro_type,
            @Field("Part_Volt") String Part_Volt
    );

    @FormUrlEncoded
    @POST("ProductsearchNew")
    Call<ProductSearch> productSearchNew(
            @Field("Productname") String Productname
    );

    @FormUrlEncoded
    @POST("OEcustomers")
    Call<OECustomers> oeCustomerList(
            @Field("Segmentss") String segmentss
    );

    @FormUrlEncoded
    @POST("Model")
    Call<ModelList> modelList(
            @Field("segment") String segment,
            @Field("make") String make
    );

    @FormUrlEncoded
    @POST("Partlist")
    Call<AppPart> appPartList(
            @Field("segment") String segment,
            @Field("make") String make,
            @Field("model") String model
    );

    @FormUrlEncoded
    @POST("NewPartsearch")
    Call<ServiceParts> newServicePartSearch(
            @Field("PartNumber") String partNumber
    );

    @FormUrlEncoded
    @POST("NewPartsearch")
    Call<ChildParts> newChildPartSearch(
            @Field("PartNumber") String partNumber
    );

    @FormUrlEncoded
    @POST("NewPartsearch")
    Call<PartSearch> newPartSearch(
            @Field("PartNumber") String partNumber
    );

    @GET("Univercellist1")
    Call<UniversalList> universalList();


    @POST("SaveOrders")
    Call<PlaceOrder> placeOrder(
            @Body OrderModel partNumber
    );

    @FormUrlEncoded
    @POST("searchpending")
    Call<PendingOrders> searchPendingNonLMUser(
            @Field("Dlr_Code") String dlrCode,
            @Field("UserType") String userType
    );

    @FormUrlEncoded
    @POST("searchpending")
    Call<PendingOrders> searchPendingLMUSer(
            @Field("DistributorCode") String distributorCode,
            @Field("DistributoruserType") String DistributoruserType
    );

    @FormUrlEncoded
    @POST("searchprocessed")
    Call<CompletedOrders> searchCompletedNonLMUser(
            @Field("Dlr_Code") String dlrCode,
            @Field("UserType") String userType
    );

    @FormUrlEncoded
    @POST("searchprocessed")
    Call<CompletedOrders> searchCompletedLMUSer(
            @Field("DistributorCode") String distributorCode,
            @Field("DistributoruserType") String DistributoruserType
    );

    @FormUrlEncoded
    @POST("orderdetailshistory")
    Call<PendingCompletedOrders> viewMore(
            @Field("OrderNumber") long OrderNumber
    );

    @FormUrlEncoded
    @POST("autocodeorder")
    Call<GetUserDetails> getUserDetails(
            @Field("UserType") String UserType
    );

    @FormUrlEncoded
    @POST("Addresslist")
    Call<GetPreviousAddress> getaddress (
            @Field("Code") String code,
            @Field("UserType") String usertype
    );
    //overall search method
    @FormUrlEncoded
    @POST("overallsearch1")
    Call<OverallSearchResult> getoverall (
            @Field("search") String searchedstring
    );
    @FormUrlEncoded
    @POST("GetBrandlist")
    Call<BrandDetails> brandname (
            @Field("BRAND") String brandname
    );

    @GET("Brandlist")
    Call<GetBrandList> brandlist();

    @FormUrlEncoded
    @POST("getbrandwiseproduct")
    Call<GetBrandWiseProduct> brandwiseproduct (
            @Field("BRAND") String retailerNo

    );

    @FormUrlEncoded
    @POST("NameOfproduct1")
    Call<PartNumberBrandWise> partnumberbranwise (
            @Field("Productname") String retailerNo
    );

    @FormUrlEncoded
    @POST("PostUserImage")
    Call<UploadImage> uploadimage (
            @Part MultipartBody.Part file
    );

//    @FormUrlEncoded
//    @POST("FeedbackForm")
//    Call<FeedBack> feedback (
//            @Field("Name") String name,
//            @Field("MobileNo") String mob,
//            @Field("Emailid") String email,
//            @Field("ComplaintType") String compty,
//            @Field("UserType") String usertype,
//            @Field("Feedback") String feedback
//
//    );

}
