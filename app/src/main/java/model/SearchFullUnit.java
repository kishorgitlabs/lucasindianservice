
package model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SearchFullUnit {

    @SerializedName("data")
    private List<SearchFullUnitList> mData;
    @SerializedName("result")
    private String mResult;

    public List<SearchFullUnitList> getData() {
        return mData;
    }

    public void setData(List<SearchFullUnitList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
