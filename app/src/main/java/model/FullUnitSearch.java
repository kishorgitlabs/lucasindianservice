
package model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class FullUnitSearch {

    @SerializedName("data")
    private List<FullUnitSearchList> mData;
    @SerializedName("result")
    private String mResult;

    public List<FullUnitSearchList> getData() {
        return mData;
    }

    public void setData(List<FullUnitSearchList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
