package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.brainmagic.lucasindianservice.demo.R;

public class WishListAdapter extends ArrayAdapter {
    private Context context;
    public WishListAdapter(@NonNull Context context) {
        super(context, R.layout.wish_list_adapter);
        this.context=context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view= LayoutInflater.from(context).inflate(R.layout.wish_list_adapter,null);
        return view;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }
}
