package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.BrandProductList;
import com.brainmagic.lucasindianservice.demo.BrandSpecification;
import com.brainmagic.lucasindianservice.demo.R;

import java.util.List;

public class BrandWiseListAdapter extends ArrayAdapter {
    Context context;
    private List<String> brand;


    public BrandWiseListAdapter(@NonNull Context context, List<String> brandname) {
        super(context, R.layout.brandadapter);
        this.context=context;
        this.brand=brandname;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView=null;

        if (convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.brandadapter,null);
            TextView brandname=convertView.findViewById(R.id.brandname);
            ImageView branimage=convertView.findViewById(R.id.branimage);
            RelativeLayout layout=convertView.findViewById(R.id.layout);
               brandname.setText(brand.get(position));

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String Brandname=brand.get(position);
                    if (Brandname.equals("TVS AUTOMOTIVE")) {
                        Intent productspec=new Intent(context, BrandSpecification.class);
                        productspec.putExtra("productname","LUBRICANT");
                        productspec.putExtra("brandclicked","TVS AUTOMOTIVE");
                        context.startActivity(productspec);
                    }
                    else if (Brandname.equals("NSK BEARING")) {
                        Intent productspec=new Intent(context, BrandSpecification.class);
                        productspec.putExtra("productname","NSK BEARING");
                        productspec.putExtra("brandclicked","NSK BEARING");
                        context.startActivity(productspec);
                    }
                    else {
                        Intent branddetails=new Intent(context, BrandProductList.class);
                        branddetails.putExtra("brand",Brandname);
                        context.startActivity(branddetails);
                    }
                }
            });
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return brand.size();
    }
}

