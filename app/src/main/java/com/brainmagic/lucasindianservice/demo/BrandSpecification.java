package com.brainmagic.lucasindianservice.demo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.BrandSpecificationAdapter;
import adapter.SegmentAdpaterTwo;
import home.Home_Activity;
import model.ModelList;
import model.ProductSearchList;
import model.getbranddetails.BrandData;
import model.getbranddetails.BrandDetails;
import model.partnumberforbrand.PartNumberBrandWise;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class BrandSpecification extends AppCompatActivity {

    private ListView branddetails;
    private String postbrandname;
    private BrandSpecificationAdapter brandSpecificationAdapter;
    private List<String> partnumberbrandproduct;
    private TextView details,brand_detail_list,brandclicked;
    private ImageView product_list_view_home,backb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_specification);
        partnumberbrandproduct=new ArrayList<>();
        branddetails=findViewById(R.id.branddetails);
        details=findViewById(R.id.details);
        brand_detail_list=findViewById(R.id.brand_detail_list);
        brandclicked=findViewById(R.id.brandclicked);
        backb=findViewById(R.id.backb);
        product_list_view_home=findViewById(R.id.product_list_view_home);
        brandclicked.setText(getIntent().getStringExtra("brandclicked"));
        postbrandname=getIntent().getStringExtra("productname");
        details.setText(postbrandname);
        if (postbrandname.equals("LUBRICANT")){
            brand_detail_list.setText("TVS AUTOMOTIVE");
        }else {
            brand_detail_list.setText(postbrandname);
        }
        getbrandnamedetails();
        product_list_view_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(), Home_Activity.class);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
        backb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void getbrandnamedetails()  {
        final ProgressDialog progressDialog = new ProgressDialog(BrandSpecification.this,
                R.style.Progress);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            CategoryAPI service = RetroClient.getApiService();
            Call<PartNumberBrandWise> call = service.partnumberbranwise(postbrandname);
            call.enqueue(new Callback<PartNumberBrandWise>() {
                @Override
                public void onResponse(Call<PartNumberBrandWise> call, Response<PartNumberBrandWise> response) {
                    if (response.body().getResult().equals("Success")) {
                        progressDialog.dismiss();
                        partnumberbrandproduct=response.body().getData();
                        brandSpecificationAdapter=new BrandSpecificationAdapter(BrandSpecification.this,partnumberbrandproduct);
                        branddetails.setAdapter(brandSpecificationAdapter);
                    }
                    else {
                        StyleableToasty styleableToasty=new StyleableToasty(BrandSpecification.this);
                        styleableToasty.showFailureToast("No Record Found Try Again Later");
                        progressDialog.dismiss();
                    }
                }
                @Override
                public void onFailure(Call<PartNumberBrandWise> call, Throwable t) {
                    progressDialog.dismiss();
                    StyleableToasty styleableToasty=new StyleableToasty(BrandSpecification.this);
                    styleableToasty.showFailureToast("No Record Found Failure Please Try Again Later");

                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
            StyleableToasty styleableToasty=new StyleableToasty(BrandSpecification.this);
            styleableToasty.showFailureToast("No Record Found Expection");
        }
    }
}
