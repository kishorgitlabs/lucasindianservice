package RetroClient;

import ApiInterface.CategoryAPI;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroClient  {


    //sap quailty link
//    private static final String ROOT_URL = "http://14.142.78.188/LucasIndianServiceQuality/LucasIndianServiceQuality/api/Value/";
    // live link
    public static final String ROOT_URL = "http://14.142.78.188/LISMOB/API/Value/";
    public static final String ROOT_URLUPLOAD = "http://14.142.78.188/LISMOB/API/Upload/";

    //brainmagic link
//    private static final String ROOT_URL = "http://lucasindianjson.brainmagicllc.com/api/Value/";

    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private static Retrofit getRetrofitUpload() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URLUPLOAD)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    /**
     * Get API Service
     *
     * @return API Service
     */
    public static CategoryAPI getApiService() {
        return getRetrofitInstance().create(CategoryAPI.class);
    }

    public static CategoryAPI getApiServiceUpload() {
        return getRetrofitUpload().create(CategoryAPI.class);
    }
}
