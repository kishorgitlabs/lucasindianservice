package com.brainmagic.lucasindianservice.demo;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.SegmentAdapter;
import home.Home_Activity;
import model.segment.SegmentSearch;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SegmentsList extends AppCompatActivity {
    private GridView segmentListView;
    private ArrayList<String> segmentlist;
    private ArrayList<String> imagelist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segments_list);

        ImageView back=findViewById(R.id.segment_back);
        ImageView home=findViewById(R.id.segment_home);
        ImageView cart=findViewById(R.id.segment_cart_details);

        segmentListView=findViewById(R.id.segment_list);


//        segmentlist = new ArrayList<>();
//        segmentlist.add("Two Wheeler");
//        segmentlist.add("Three Wheeler");
//        segmentlist.add("CAR");
//        segmentlist.add("UV");
//        segmentlist.add("LCV");
//        segmentlist.add("HCV");
//        segmentlist.add("Tractor");
//        segmentlist.add("Engine");
//        segmentlist.add("Equipment");
//        segmentlist.add("Marine");
//        segmentlist.add("Defense");
//        segmentlist.add("Locomotive");

//        SegmentAdapter segmentAdapter=new SegmentAdapter(getApplicationContext(),segmentlist);
//        segmentListView.setAdapter(segmentAdapter);

        NetworkConnection networkConnection=new NetworkConnection(SegmentsList.this);
        if(networkConnection.CheckInternet())
        {
            //after internet checking get the segment list
            getSegmentView();
        }
        else {
            Toast.makeText(getApplicationContext(),"Check  your Network Connection",Toast.LENGTH_SHORT).show();
        }

        segmentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Intent intent=new Intent(getApplicationContext(),SegmentsDetailsTwo.class);
//                intent.putExtra("segment",segmentlist.get(i));
                startActivity(new Intent(getApplicationContext(),SegmentsDetailsTwo.class).putExtra("segment",segmentlist.get(i)));

//                ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(SegmentsList.this,
//                        new Pair<View, String>(view.findViewById(R.id.grid_text), "segment"));
//
//                ActivityCompat.startActivity(SegmentsList.this, intent, activityOptionsCompat.toBundle());

            }
        });

        //goto previous activity
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //to goto home page
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SegmentsList.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });

        //to get cart items
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SegmentsList.this,OrderActivity.class));
            }
        });
    }

    //get segment method json
    public void getSegmentView()
    {
        try {
            CategoryAPI service = RetroClient.getApiService();
            Call<SegmentSearch> call = service.segmentList();
            call.enqueue(new Callback<SegmentSearch>() {
                @Override
                public void onResponse(Call<SegmentSearch> call, Response<SegmentSearch> response) {
                    //segment success
                    if (response.body().getResult().equals("Success")) {
                        segmentlist = new ArrayList<>();
                        imagelist = new ArrayList<>();
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            segmentlist.add(response.body().getData().get(i).getSegmentName());
                            imagelist.add(response.body().getData().get(i).getSegmentImage());
                        }
                        SegmentAdapter segmentAdapter = new SegmentAdapter(getApplicationContext(), segmentlist, imagelist);
                        segmentListView.setAdapter(segmentAdapter);
                    } else {
                        Toast.makeText(getApplicationContext(), "Data Not Success", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<SegmentSearch> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Failed to reach the server", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
