package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.lucasindianservice.demo.R;

import java.util.List;

import model.orderhistory.PendingOrderList;
import orders.PendingCompletedOrders;

public class PendingAdapter extends ArrayAdapter {

    private Context context;
    private List<PendingOrderList> data;

    public PendingAdapter(@NonNull Context context,List<PendingOrderList> data) {
        super(context, R.layout.pending_order_adapter);
        this.context=context;
        this.data=data;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view= LayoutInflater.from(context).inflate(R.layout.pending_order_adapter,null);
        TextView sNo=view.findViewById(R.id.pending_order_s_no);
        TextView orderNo=view.findViewById(R.id.pending_order_number);
        TextView dlrCode=view.findViewById(R.id.pending_order_dlr_code);
        TextView date=view.findViewById(R.id.pending_order_date);
        TextView totalAmount=view.findViewById(R.id.pending_order_total_amount);
        TextView status=view.findViewById(R.id.pending_order_status);
        ImageView viewMore=view.findViewById(R.id.pending_order_view_more);

        sNo.setText(position+1+"");
        orderNo.setText(String.valueOf(data.get(position).getOrderNumber()));
        dlrCode.setText(String.valueOf(data.get(position).getDLRCode()));
        String[] tempDate={};
        if(data.get(position).getOrderDate().contains("T"))
        {
            tempDate=data.get(position).getOrderDate().split("T");
        }
        date.setText(tempDate[0]);
        totalAmount.setText("Rs"+(data.get(position).getTotalOrderAmount()));
        status.setText(String.valueOf(data.get(position).getOrderStatus()));

        viewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, PendingCompletedOrders.class);
                intent.putExtra("orderType","Pending");
                intent.putExtra("orderNumber",data.get(position).getOrderNumber());
                context.startActivity(intent);
            }
        });


        return view;
    }

    @Override
    public int getCount() {
        return data.size();
    }
}
