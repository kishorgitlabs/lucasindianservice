package com.brainmagic.lucasindianservice.demo;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.PartTableRecyclerView;
import alertbox.Alertbox;
import home.Home_Activity;
import model.AppPart;
import model.AppPartList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApplicationTableActivity extends AppCompatActivity {
    private String make,model,segment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_application_table);

        make=getIntent().getStringExtra("make");
        model=getIntent().getStringExtra("model");
        segment=getIntent().getStringExtra("segment");
        ImageView back=findViewById(R.id.application_table_back);
        ImageView home=findViewById(R.id.application_table_home);
        TextView partDetailMakeRoot=findViewById(R.id.detail_make);
        TextView models=findViewById(R.id.detail);
        ImageView cart=findViewById(R.id.table_cart_details);
        TextView segments=findViewById(R.id.detail_application);
        TextView universalParts=findViewById(R.id.universal_parts);
        TextView applicationProduct=findViewById(R.id.application_product);
        applicationProduct.setText(model);
        partDetailMakeRoot.setText(make);
        models.setText(model);
        segments.setText(segment);

        universalParts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ApplicationTableActivity.this,UniversalActivity.class));
            }
        });

//        TextView textView=findViewById(R.id.part_info);
//        textView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(ApplicationTableActivity.this,PartDetails.class));
//            }
//        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ApplicationTableActivity.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ApplicationTableActivity.this,OrderActivity.class));
            }
        });
        getPartList();
//        new GetPartlist().execute("");
    }


    //get the part as a list in data
    private void getPartList()
    {
        try
        {
            final ProgressDialog progressDialog = new ProgressDialog(ApplicationTableActivity.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            CategoryAPI service = RetroClient.getApiService();
            Call<AppPart> call=service.appPartList(segment,make,model);

            call.enqueue(new Callback<AppPart>() {
                @Override
                public void onResponse(Call<AppPart> call, Response<AppPart> response) {
                    if(response.body().getResult().equals("success"))
                    {
                        List<AppPartList> partList=response.body().getData();
                        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                        RecyclerView recyclerView=findViewById(R.id.card_recycler_view);
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(new PartTableRecyclerView(ApplicationTableActivity.this,partList));
                        progressDialog.dismiss();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "No Record Found", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<AppPart> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "No Record Found", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),"Error Occured Please try again later...!",Toast.LENGTH_SHORT).show();
        }


    }

//    public class GetPartlist extends AsyncTask<String, Void, String>{
//
//        final ProgressDialog progressDialog = new ProgressDialog(ApplicationTableActivity.this,
//                R.style.Progress);
//        private List<AppPartList> partList;
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog.setIndeterminate(true);
//            progressDialog.setMessage("Loading...");
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            try{
//                CategoryAPI service = RetroClient.getApiService();
//                Call<AppPart> call=service.appPartList(segment,make,model);
//
//                call.enqueue(new Callback<AppPart>() {
//                    @Override
//                    public void onResponse(Call<AppPart> call, Response<AppPart> response) {
//                        if(response.body().getResult().equals("success"))
//                        {
//                            partList=response.body().getData();
//                        }
//                        else
//                        {
//                            Toast.makeText(getApplicationContext(), "No Record Found", Toast.LENGTH_LONG).show();
//                            progressDialog.dismiss();
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<AppPart> call, Throwable t) {
//                        Toast.makeText(getApplicationContext(), "No Record Found", Toast.LENGTH_LONG).show();
//                        progressDialog.dismiss();
//                    }
//                });
//            }catch (Exception e)
//            {
//                return "error";
//            }
//            return "success";
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            if(s.equals("success"))
//            {
//                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
//                RecyclerView recyclerView=findViewById(R.id.card_recycler_view);
//                recyclerView.setLayoutManager(layoutManager);
//                recyclerView.setAdapter(new PartTableRecyclerView(ApplicationTableActivity.this,partList));
//                progressDialog.dismiss();
//            }
//            else {
//                Alertbox alertbox=new Alertbox(ApplicationTableActivity.this);
//                alertbox.showNegativebox("Server Error, Please Try again later");
//            }
//        }
//    }



}
